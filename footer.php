
        <!-- footer part -->
            <footer>
                <div class="container-fluid tic-footercontent">
                    
                <div class="row">
                    <div class="col-sm-3 col-xs-6">
                        <div class="text-left">
                            <a href="#"><img src="India/Images/Animated_Images/animated-india-flag.gif" class="img-responsive" width="100px;"/></a>
                            <p>The Indian Cultures</p>
                            
                            <p>भारतीय संस्कृति</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <h3 class="tic-footerheading">About Us</h3>

                        <div class="tic-abtuslink">
                            <ul>
                                <li><a href="#">Events</a></li>
                                <li><a href="#">Festivals</a></li>
                                <li><a href="#">News</a></li>
                                <li><a href="#">Gallery</a></li>
                                <li><a href="#">Blogs</a></li>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Contact Us</a></li>
                            </ul>
                        </div>

                    </div>
                    <div class="col-sm-3">
                        <h3 class="tic-footerheading">Subscribe Us</h3>
                    </div>
                    <div class="col-sm-3 col-xs-6">
                        <h3 class="tic-footerheading">Follow Us</h3>
                        <div class="tic-socialicon">
                            <a href="https://www.facebook.com/Theindiancultures-293072311244348/" target="bl"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="https://twitter.com/theindncultures" target="bl"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="https://www.linkedin.com/in/theindiancultures-tic-04056816a/" target="bl"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                            <a href="https://www.instagram.com/theindianculturestic/" target="bl"><i class="fa fa-instagram" aria-hidden="true"></i></a>            
                            <a href="https://plus.google.com/117158300348145104053" target="bl"><i class="fa fa-google-plus" aria-hidden="true"></i></a>            
                            <a href="#" target="bl"><i class="fa fa-rss" aria-hidden="true"></i></a>
                            <a href="#" target="bl"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    
                </div>

            </div>

                <div class="container-fluid tic-footercpyrt">

                <div class="row">
                    <div class="col-sm-12  text-center">
                        <p>Copyright &copy; The Indian Cultures 2018 <span>Made with <span class="tic-mwlhearticon"><i class="fa fa-heart" aria-hidden="true"></i></span> in india!</span></p>
                    </div>
                </div>

                </div>

            </footer>
        <!-- End of footer part -->