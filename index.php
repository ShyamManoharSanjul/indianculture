<!DOCTYPE html>
<html>
    <head>
    
        <title>The Indian Cultures</title>
            
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="stylesheet" type="text/css" href="bootstrap-3.3.7-dist/css/bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="font-awesome-4.7.0/css/font-awesome.css" />
        
        
        <script src="JS/jquery.js"></script>
        <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
        
      
            
        <link rel="icon" type="image/png" href="India/Images/icon/India-Flag.png"/>
            
        <link rel="stylesheet" type="text/css" href="Styles/common.css"/>  
        <link rel="stylesheet" type="text/css" href="Styles/main.css"/>
        <link rel="stylesheet" type="text/css" href="Animation/animate.css"/>
        
    </head>

    <body>
        <header>
            <?php
            include("header.php");
            ?>
        </header>
        
        <main>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3">   
                         <?php
                        include("lefter.php");
                        ?>               
                    </div>
                    
                    <div class="col-sm-9">

                        <h3 class="tic-mainheading text-center">Republic of India  - [ Bhārat Gaṇarājya ] </h3>
                        
                        <div class="row">
                            <div class="col-xs-4">
                                <img src="India/Images/images/glob.png" width="100px" height="100px;" alt="Glob"/>
                            </div>
                            <div class="col-xs-4">
                                <img src="India/Images/Animated_Images/india-map.gif" width="100px" height="100px;" alt="India"/>
                            </div>
                            <div class="col-xs-4">
                                <img src="India/Images/images/indiamap.jpg"  width="100px" height="100px;" alt="India Map"/>
                            </div>
                        </div>  
                        <hr/>
                         <div class="row">
                            <div class="col-sm-4 fonts">
                                <b>Flag</b>
                            </div>
                            <div class="col-sm-4">
                                <img src="India/Images/images/India_flag.gif" class="img-responsive" width="100px" alt="Indian Flag"/>
                            </div>
                            <div class="col-sm-4 text-justify">
                                <p><b>Colours of the Flag:</b> In the national flag of India the top band is of Saffron colour, indicating the strength and courage of the country. The white middle band indicates peace and truth with Dharma Chakra. The last band is green in colour shows the fertility, growth and auspiciousness of the land.</p>
                            </div>
                        </div>  
                        <hr/>
                    
                        <div class="row">
                            <div class="col-sm-4 fonts">
                                <b>National Emblem</b>
                            </div>
                            <div class="col-sm-4">
                                <img src="India/Images/images/indianemblem.png"  width="100px"  alt="Indian Emblem" class="img-thumbnail"/>
                            </div>
                            <div class="col-sm-4">
                               <p> Motto: "Satyameva Jayate" (Sanskrit)</p>
                            </div>
                        </div>
                        <hr/>
                        
                        <div class="row">
                            <div class="col-sm-4 fonts">
                                <b>Anthem</b>
                            </div>
                            <div class="col-sm-4">
                                <audio controls>
                                    <source src="India/anthem/Indian_National_Anthem_-_Flute_Instrumental.mp3" type="audio/mpeg">
                                </audio>
                            </div>
                            <div class="col-sm-4">
                               <p> "Jana Gana Mana" (Hindi)</p>
                            </div>
                        </div>
                        <hr/>
                        
                        <div class="row">
                            <div class="col-sm-4 fonts">
                                <b>National song</b>
                            </div>
                            <div class="col-sm-4">
                                <audio controls>
                                    <source src="India/national_song/Vande_Mataram_National_Song_Of_india.mp3" type="audio/mpeg">
                                </audio>
                            </div>
                            <div class="col-sm-4">
                                <p>"Vande Mataram" (Hindi)</p>
                            </div>
                        </div>
                        <hr/>
                        
                        <div class="row">
                            <div class="col-sm-4 fonts">
                                <b>Capital</b>
                            </div>
                            <div class="col-sm-4">
                                <img src="India/Images/images/NewDelhiMontage.png"  width="100px"  alt="New Delhi" class="img-thumbnail"/>
                            </div>
                            <div class="col-sm-4 text-justify">
                                <b>New Delhi</b>
                                <p>New Delhi is the capital of India and one of Delhi city's 11 districts.</p>
                            </div>
                        </div>
                        <hr/>
                        
                        <div class="row">
                            <div class="col-sm-4 fonts">
                                <b>Largest city</b>
                            </div>
                            <div class="col-sm-4">
                                <img src="India/Images/images/MumbaiMontage.png"  width="100px"  alt="Mumbai" class="img-thumbnail"/>
                            </div>
                            <div class="col-sm-4 text-justify">
                                <b>Mumbai </b>
                                <p>Mumbai also known as Bombay, the official name until 1995) is the capital city of the Indian state of Maharashtra.</p>
                            </div>
                        </div>
                        <hr/>
                        
                        
                        
                        <div class="row">
                            <div class="col-sm-4 fonts">
                                <b>Official languages</b>
                            </div>
                            <div class="col-sm-4 text-justify">
                                <b>Hindi (हिन्दी)</b>
                                <p>Hindi (Devanagari: हिन्दी), or Modern Standard Hindi (Devanagari: मानक हिन्दी: Mānak Hindī) is a standardised and Sanskritised register of the Hindustani language.</p>
                            </div>
                            <div class="col-sm-4 text-justify">
                                <b>English</b>
                                <p>Indian English is any of the forms of English characteristic of India. English is a lingua franca of India.</p>
                            </div>
                        </div>
                        <hr/>
                        
                        <div class="row">
                            <div class="col-sm-4 fonts">
                                <b>Religion</b>
                            </div>
                            <div class="col-sm-4 text-justify">
                                <p>79.8% Hinduism</p>
                                <p>14.2% Islam</p>
                                <p>2.3% Christianity</p>
                            </div>
                            <div class="col-sm-4 text-justify">
                                <p>1.7% Sikhism</p>
                                <p>0.7% Buddhism</p>
                                <p>0.4% Jainism</p>
                                <p>0.9% others</p>
                            </div>
                        </div>
                        <hr/>
                        
                        <div class="row">
                            <div class="col-sm-4 fonts">
                                <b>Demonym</b>
                            </div>
                            <div class="col-sm-4 text-justify">
                                <p><b>Indian</b></p>
                                <img src="India/Images/images/indian.jpg"  width="100px" height="100" alt="Indian Hand" class="img-thumbnail"/>
                            </div>
                            <div class="col-sm-4 text-justify">
                                <b>Indian</b>
                                <p>A demonym is a word that identifies residents or natives of a particular place, which is derived from the name of that particular place.</p>
                                <p>Examples of demonyms include Indian for a person of India.</p>
                            </div>
                        </div>
                        <hr/>
                        
                        <div class="row">
                            <div class="col-sm-4 fonts">
                                <b>Government</b>
                            </div>
                            <div class="col-sm-8 text-justify ic-imghovereffect">
                                <p><img src="India/Images/images/president.PNG"  width="50px" height="50" alt="Ram Nath Kovind" class="img-thumbnail"/><b>&nbsp; Ram Nath Kovind (President)</b></p>
                                
                                <p><img src="India/Images/images/vicepresident.PNG"  width="50px" height="50" alt="Mumbai" class="img-thumbnail"/><b>&nbsp; Venkaiah Naidu (Vice-President)</b></p>
                                
                                <p><img src="India/Images/images/primeminister.jpg"  width="50px" height="50" alt="Narendra Damodardas Modi" class="img-thumbnail"/><b>&nbsp; Narendra Damodardas Modi (Prime Minister)</b></p>
                                
                                <p><img src="India/Images/images/chiefjustice.PNG"  width="50px" height="50" alt="Dipak Misra" class="img-thumbnail"/><b>&nbsp; Dipak Misra (Chief Justice)</b></p>
                                
                                <p><img src="India/Images/images/speakeroftheloksabha.jpg"  width="50px" height="50" alt="Sumitra Mahajan" class="img-thumbnail"/><b>&nbsp; Sumitra Mahajan (Lok Sabha Speaker)</b></p>
                            </div>
                        </div>
                        <hr/>
                        
                        <div class="row">
                            <div class="col-sm-4 fonts">
                                <b>Legislature</b>
                            </div>
                            <div class="col-sm-4 text-justify">
                                <p><b>Parliament of India</b></p>
                                    <img src="India/Images/images/Indian-Parliament-House-Delhi.jpg"  width="140px" alt="Indian-Parliament" class="img-thumbnail"/>
                            </div>
                            <div class="col-sm-4 text-justify">
                                <p>Rajya Sabha</p>
                                <p>Lok Sabha</p>
                            </div>
                        </div>
                        <hr/>
                        
                        <div class="row">
                            <div class="col-sm-4 fonts">
                                <b>Independence</b>
                            </div>
                            <div class="col-sm-4 text-justify">
                                    <img src="India/Images/images/indian_flag_nice.jpg"  width="140px" alt="Indian-Parliament" class="img-thumbnail"/>
                            </div>
                            <div class="col-sm-4 text-justify">
                                <p>Dominion - [15 August 1947]</p>
                                <p>Republic - [26 January 1950]</p>
                            </div>
                        </div>
                        <hr/>

                        <div class="row">
                            <div class="col-sm-4 fonts">
                                <b>Currency</b>
                            </div>
                            <div class="col-sm-4 text-justify">
                                    <img src="India/Images/images/indian_currency.jpg"  width="140px" alt="Indian-Parliament" class="img-thumbnail"/>
                            </div>
                            <div class="col-sm-4 text-justify">
                                <p>Indian Rupee (INR)</p>
                            </div>
                        </div>
                        <hr/>
                        
                        <div class="row">
                            <div class="col-sm-4 fonts">
                                <b>Population</b>
                            </div>
                            <div class="col-sm-4 text-justify">
                                    <b>2017 estimate</b>
                            </div>
                            <div class="col-sm-4 text-justify">
                                <p>1,310,069,000</p>
                            </div>
                        </div>
                        <hr/>
         </div> <!--End of col-9-->
                    
                </div> <!--End of row-->
            </div> <!--End of container-->
            
<div class="container-fluid tic-culturebox">
                        
                        <div class="row">
                            <div class="col-sm-4">
                               
                            </div>
                            <div class="col-sm-4 text-center">
                                   <h2 class="tic-culturalheading">Culture of India</h2>
                            </div>
                            <div class="col-sm-4">
                                
                            </div>
                        </div>
                        

  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#our_india">Our India</a></li>
    <li><a data-toggle="tab" href="#national_symbols">National Symbols</a></li>
    <li><a data-toggle="tab" href="#family_structure_and_marriage">Family structure and marriage</a></li>
    <li><a data-toggle="tab" href="#greetings">Greetings</a></li>
    <li><a data-toggle="tab" href="#festivals">Festivals</a></li>
    <li><a data-toggle="tab" href="#animals">Animals</a></li>
    <li><a data-toggle="tab" href="#cuisine">Cuisine</a></li>
    <li><a data-toggle="tab" href="#clothing">Clothing</a></li>
    <li><a data-toggle="tab" href="#languages_and_literature">Languages and literature</a></li>
    <li><a data-toggle="tab" href="#performing_arts_and_visual_arts">Performing arts & Visual arts</a></li>
    
  </ul>

  <div class="tab-content tic-tabtitleheading">
      
      <!--Start of Our India Container-->
      
    <div id="our_india" class="tab-pane fade in active text-justify">
      <h3>Our India</h3>
        <div class="row text-center">
            <div class="col-sm-4">
                <img src="India/Images/images/our_indiamap.png"  width="250px" height="200px" alt="Our India"/>
            </div>
            <div class="col-sm-4">
                <img src="India/Images/Animated_Images/india-map.gif"  width="250px" height="200px" alt="Our India"/>
            </div>
            <div class="col-sm-4">
                <img src="India/Images/Animated_Images/india_map_history.gif"  width="250px" height="200px" alt="Our India"/>
            </div>
        </div>
        <hr/>
           
        <div class="row">
            <div class="col-sm-6">
                <p>India is one of the world's oldest civilizations and one of the most populated countries in the world. The Indian culture, often labeled as an amalgamation of several various cultures, spans across the Indian subcontinent and has been influenced and shaped by a history that is several thousand years old. Throughout the history of India, Indian culture has been heavily influenced by Dharmic religions. They have been credited with shaping much of Indian philosophy, literature, architecture, art and music.Greater India was the historical extent of Indian culture beyond the Indian subcontinent. This particularly concerns the spread of Hinduism, Buddhism, architecture, administration and writing system from India to other parts of Asia through the Silk Road by the travellers and maritime traders during the early centuries of the Common Era. To the west, Greater India overlaps with Greater Persia in the Hindu Kush and Pamir Mountains. Over the centuries, there has been significant fusion of cultures between Buddhists, Hindus, Muslims (Sunni, Shia, Sufi), Jains, Sikhs and various tribal populations in India.</p>
                
            </div>
            <div class="col-sm-6">
                <p>भारत विश्व की सबसे पुरानी सभ्यताओं में से एक है और दुनिया के सबसे अधिक आबादी वाले देशों में से एक है। भारतीय संस्कृति, अक्सर भारतीय उपमहाद्वीप में कई विभिन्न संस्कृतियों के एकीकरण के रूप में लेबल की जाती है और इतिहास द्वारा प्रभावित और आकार आती है जो कई हजार साल पुराना है। भारत के इतिहास के दौरान, भारतीय संस्कृति पर धर्म के धर्मों पर भारी प्रभाव पड़ा है। उन्हें भारतीय दर्शन, साहित्य, वास्तुकला, कला और संगीत के बहुत सारे आकार देने का श्रेय दिया गया है। भारतीय उपमहाद्वीप से परे भारतीय संस्कृति की ऐतिहासिक सीमा ऐतिहासिक थी। यह विशेष रूप से आम युग की शुरुआती शताब्दियों के दौरान यात्रियों और समुद्री व्यापारियों द्वारा सिल्क रोड के माध्यम से भारत के हिंदुओं, बौद्ध धर्म, वास्तुकला, प्रशासन और लेखन प्रणाली के प्रसार को लेकर एशिया के दूसरे हिस्सों के बारे में चिंतित है। पश्चिम में, ग्रेटर भारत हिंदू कुश और पामीर पर्वत में ग्रेटर फारस के साथ ओवरलैप करता है। सदियों से, भारत में बौद्ध, हिंदू, मुस्लिम (सुन्नी, शिया, सूफी), जैन, सिख और विभिन्न जनजातीय आबादी के बीच संस्कृतियों का महत्वपूर्ण संलयन रहा है।</p>
                
            </div>
           </div>
        <hr/>
        
        <div class="row">
            <div class="col-sm-6">
                <p>India is the birthplace of Hinduism, Buddhism, Jainism and Sikhism, collectively known as Indian religions.Indian religions are a major form of world religions along with Abrahamic ones. Today, Hinduism and Buddhism are the world's third and fourth-largest religions respectively, with over 2 billion followers altogether, and possibly as many as 2.5 or 2.6 billion followers. Followers of Indian religions – Hindus, Sikhs, Jains and Buddhists make up around 80–82% population of India.</p>
            </div>
            <div class="col-sm-6">
                <p>भारत हिंदू धर्म, बौद्ध धर्म, जैन धर्म और सिख धर्म का जन्मस्थान है, सामूहिक रूप से भारतीय धर्मों के रूप में जाना जाता है। भारतीय धर्म अब्राहमियों के साथ विश्व धर्मों का एक बड़ा रूप है। आज, हिंदू धर्म और बौद्ध धर्म विश्व के तीसरे और चौथे सबसे बड़े धर्म हैं, क्रमशः 2 अरब से अधिक अनुयायियों के साथ, और संभवत: 2.5 या 2.6 अरब अनुयायी। भारतीय धर्म के अनुयायी - हिंदुओं, सिख, जैन और बौद्ध भारत के लगभग 80-82% आबादी का हिस्सा हैं</p>
            </div>
        </div>
        
        <hr/>
            
    </div>
      
      <!--End of Our India Container-->
      
      
      
      <!--Start of National Symbols Container-->
      
    <div id="national_symbols" class="tab-pane fade">
      <h3>National Symbols</h3>
      
                      <div class="row">
                            <div class="col-sm-3 fonts">
                                <b>National Flower</b>
                            </div>
                            <div class="col-sm-3 text-justify">
                                    <b>Indian Lotus</b>
                            </div>
                            <div class="col-sm-3 text-justify">
                                    <img src="India/Images/images/national_flower_lotus.jpg"  width="140px" alt="National Flower Lotus" class="img-thumbnail"/>
                            </div>
                            <div class="col-sm-3 text-justify">
                                <p>Lotus (Nelumbo nucifera) is a sacred flower and occupies a unique position in the art and mythology of ancient India and has been an auspicious symbol of Indian culture.</p>
                            </div>
                        </div>
                        <hr/>
        
                    <div class="row">
                            <div class="col-sm-3 fonts">
                                <b>National Fruit</b>
                            </div>
                            <div class="col-sm-3 text-justify">
                                    <b>Mango</b>
                            </div>
                            <div class="col-sm-3 text-justify">
                                    <img src="India/Images/images/National_Fruit_Mango.jpg"  width="140px" alt="National Flower Lotus" class="img-thumbnail"/>
                            </div>
                            <div class="col-sm-3 text-justify">
                                <p>Mango (Mangifera indica) originated in India and the country is home to more than 100 varieties of the fruit.</p>
                            </div>
                        </div>
                        <hr/>
        
                <div class="row">
                            <div class="col-sm-3 fonts">
                                <b>National River</b>
                            </div>
                            <div class="col-sm-3 text-justify">
                                    <b>Ganga</b>
                            </div>
                            <div class="col-sm-3 text-justify">
                                    <img src="India/Images/images/national_river_ganga.jpg"  width="140px" alt="National Flower Lotus" class="img-thumbnail"/>
                            </div>
                            <div class="col-sm-3 text-justify">
                                <p>Ganga is the longest river of India with the most heavily populated river basin in the world. The river is revered by Hindus as the most sacred river on earth.</p>
                            </div>
                        </div>
                        <hr/>
        
            <div class="row">
                            <div class="col-sm-3 fonts">
                                <b>National Tree</b>
                            </div>
                            <div class="col-sm-3 text-justify">
                                    <b>Indian banyan</b>
                            </div>
                            <div class="col-sm-3 text-justify">
                                    <img src="India/Images/images/national_tree.jpg"  width="140px" alt="National Flower Lotus" class="img-thumbnail"/>
                            </div>
                            <div class="col-sm-3 text-justify">
                                <p>Indian banyan (Ficus bengalensis) root themselves to form new trees and grow over large areas. Because of this characteristic and its longevity, this tree is considered immortal and is an integral part of the myths and legends of India.</p>
                            </div>
                        </div>
                        <hr/>
        
        <div class="row">
                            <div class="col-sm-3 fonts">
                                <b>National Animal</b>
                            </div>
                            <div class="col-sm-3 text-justify">
                                    <b>Royal Bengal Tiger</b>
                            </div>
                            <div class="col-sm-3 text-justify">
                                    <img src="India/Images/images/national_animal_tiger.jpg"  width="140px" alt="National Flower Lotus" class="img-thumbnail"/>
                            </div>
                            <div class="col-sm-3 text-justify">
                                <p>Bengal tiger (Panthera tigris tigris), the largest carnivore is found only in the Indian subcontinent and can be found in most regions of the country.</p>
                            </div>
                        </div>
                        <hr/>
        <div class="row">
                            <div class="col-sm-3 fonts">
                                <b>National Aquatic Animal</b>
                            </div>
                            <div class="col-sm-3 text-justify">
                                    <b>River Dolphin</b>
                            </div>
                            <div class="col-sm-3 text-justify">
                                    <img src="India/Images/images/national_animal_dolphin.jpg"  width="140px" alt="National Flower Lotus" class="img-thumbnail"/>
                            </div>
                            <div class="col-sm-3 text-justify">
                                <p>Gangetic dolphin (Platanista gangetica) is said to represent the purity of the holy Ganga River as it can only survive in pure and fresh water.</p>
                            </div>
                        </div>
                        <hr/>
        
        <div class="row">
                            <div class="col-sm-3 fonts">
                                <b>National Bird</b>
                            </div>
                            <div class="col-sm-3 text-justify">
                                    <b>Indian Peacock</b>
                            </div>
                            <div class="col-sm-3 text-justify">
                                    <img src="India/Images/Animated_Images/national_bird_peacock.gif"  width="140px" alt="National Flower Lotus" class="img-thumbnail"/>
                            </div>
                            <div class="col-sm-3 text-justify">
                                <p>Indian peacock (Pavo cristatus) is designated as the national bird of India. A bird indigenous to the subcontinent, peacock represents the unity of vivid colours and finds references in Indian culture.</p>
                            </div>
                        </div>
                        <hr/>
        
    </div>
      
      <!--End of National Symbols Container-->
      
      
      
      <!--Start of Family Structure And Marriage Container-->
      
      
   <div id="family_structure_and_marriage" class="tab-pane fade">
      <h3>Family Structure And Marriage</h3>
      
                      <div class="row text-center col_padding">
                            <div class="col-sm-4 fonts">
                                <b>Marriage (Vivaah)</b>
                            </div>
                            <div class="col-sm-4">
                                    <img src="India/Images/images/indian-marriage-custom.png"  width="140px" alt="Indian Marriage" class=""/>
                                    <img src="India/Images/images/marrige.png"  width="200px" height="100px" alt="Indian Marriage Palkhi" class=""/>
                            </div>
                            <div class="col-sm-4 text-justify">
                                <p>A Hindu wedding is Vivaha (Sanskrit: विवाह) and the wedding ceremony is called vivaah sanskar in North India and kalyanam (generally) in South India. Hindus attach a great deal of importance to marriages. The ceremonies are very colourful, and celebrations may extend for several days. The bride's and groom's home - entrance, doors, wall, floor, roof - are sometimes decorated with colors, balloons,and other decorations.</p>
                            </div>
                        </div>
                        <hr/>
       
                        <div class="row text-center">
                            <div class="col-sm-12 fonts">
                                <b>Type Of Marriage</b>
                            </div>
                        </div>
                        <hr/>
       
                        <div class="row text-center col_padding">
                            <div class="col-sm-3">
                                <img src="India/Images/images/Brahma_Vivah.jpg"  width="200px" alt="Indian Marriage" class=""/>
                                
                                <br/>
                                <b>Brahma Vivaah</b>
                                <br/>
                                
                                <p class="text-justify">It has the most supreme type of matrimony among all the eight types of marriages. In this type of marriage, the groom's family search for a suitable girl for their boy. Then the father of the bride invites the potential groom to his house. After making sure that the groom is a learned man and is of good conduct, the father gets his daughter married to him.</p>
                                
                            </div>
                            <div class="col-sm-3">  
                                <img src="India/Images/images/Prajapatya_Vivah.jpg"  width="200px" alt="Indian Marriage" class=""/>
                                
                                <br/>
                                <b>Prajapatya Vivaah</b>
                                <br/>
                                
                                <p class="text-justify">In this kind of marriage, the girl's father goes in search of a suitable groom. Since here the girl's father goes in search of a suitable guy, it is also considered as an inferior type of marriage.</p>
                                
                            </div>
                            <div class="col-sm-3">
                                <img src="India/Images/images/Daiva_Vivah.jpg"  width="200px" alt="Indian Marriage" class=""/>
                                
                                <br/>
                                 <b>Daiva Vivaah</b>
                                <br/>
                                
                                <p class="text-justify">It is an inferior type of marriage. The bride's family wait for a specific time to get her married. In that time if she is not able to find a suitable groom for herself, then she is married off to a priest during a sacrifice.</p>
                                
                            </div>
                            <div class="col-sm-3">   
                                <img src="India/Images/images/Arsha_Vivah.jpg"  width="200px" alt="Indian Marriage" class=""/>
                                
                                <br/>
                                 <b>Arsha Vivaah</b>
                                <br/>
                                
                                <p class="text-justify">In this type of marriage, the girl is married to the sages. The bride is given in the exchange of two cows. Since this type of marriage involves business transactions, it is not considered a noble matrimony.</p>
                                
                            </div>
                        </div>
                        <hr/>
       
                         <div class="row text-center col_padding">
                            <div class="col-sm-3">
                                <img src="India/Images/images/Asur_Vivah.jpg"  width="200px" alt="Indian Marriage" class=""/>
                                
                                <br/>
                                <b>Asura Vivaah</b>
                                <br/>
                                
                                <p class="text-justify">In this type of marriage, the girl's family receives gifts and money from the groom. Due to this most of the times the groom is no match for the bride. But since the family receives money, the girl is forced to marry the mismatched groom.</p>
                                
                            </div>
                            <div class="col-sm-3">
                                <img src="India/Images/images/Gandharv_Vivah.jpg"  width="200px" alt="Indian Marriage" class=""/>
                                
                                <br/>
                                 <b>Gandharva Vivaah</b>
                                <br/>
                                
                                <p class="text-justify">The modern form of this type of marriage is love marriage. A boy and girl get married in secret irrespective of whether the families agree to it or not.</p>
                                
                            </div>
                            <div class="col-sm-3">
                                <img src="India/Images/images/Rakshas_Vivah.jpg"  width="200px" alt="Indian Marriage" class=""/>
                                
                                <br/>
                                 <b>Rakshasa Vivaah</b>
                                <br/>
                                
                                <p class="text-justify">In this type of marriage, the groom fights the bride's family. He forcibly marries her against her will and takes her away.</p>
                                
                            </div>
                            <div class="col-sm-3">
                                <img src="India/Images/images/Pishach_Vivah.jpg"  width="200px" alt="Indian Marriage" class=""/>
                                
                                <br/>
                                 <b>Paishacha Vivaah</b>
                                <br/>
                                
                                <p class="text-justify">In this a man stealthily seduces the girl and marries her when she is sleeping or is intoxicated or handicapped.</p>
                                
                            </div>
                        </div>
                        <hr/>
       
                        <div class="row text-center col_padding">
                            <div class="col-sm-4">
                                <b>Arranged marriage</b>
                            </div>
                            <div class="col-sm-4">
                                <img src="India/Images/images/Arrange_marriage.jpg"  width="200px" alt="Indian Marriage" class=""/>
                            </div>
                            <div class="col-sm-4">
                                 <p class="text-justify">Arranged marriages have long been the norm in Indian society. Even today, the majority of Indians have their marriages planned by their parents and other respected family-members. In the past, the age of marriage was young. The average age of marriage for women in India has increased to 21 years, according to 2011 Census of India. In 2009, about 7% of women got married before the age of 18.</p>
                            </div>
                        </div>
                        <hr/>
       
                    <div class="row text-center col_padding">
                            <div class="col-sm-4">
                                <b>Marriage in Islam (Nikah)</b>
                            </div>
                            <div class="col-sm-4">
                                <img src="India/Images/images/muslime_marriage.jpg"  width="200px" alt="Indian Marriage" class=""/>
                            </div>
                            <div class="col-sm-4">
                                 <p class="text-justify">In Islam, marriage is a legal contract (Literary Arabic: عقد القران ʻaqd al-qirān, "matrimony contract"; Urdu: نکاح نامہ‎ / ALA-LC: Nikāḥ-nāmah),(In Persian (Farsi): ازدواج (ezdevāj) (= marriage) and سند ازدواج or عقدنامه (Sǎnǎde ezdevāj; aqd nāmeh) for the certificate), between two people. Both the groom and the bride are to consent to the marriage of their own free wills.</p>
                            </div>
                        </div>
                        <hr/>
       
                    <div class="row text-center col_padding">
                            <div class="col-sm-4">
                                <b>Sikh marriage</b>
                            </div>
                            <div class="col-sm-4">
                                <img src="India/Images/images/sikh_marriage.jpg"  width="200px" alt="Indian Marriage" class=""/>
                            </div>
                            <div class="col-sm-4">
                                 <p class="text-justify">Most families combine the wedding ceremony with the engagement ceremony called the "kurmai", which is held just before the wedding vows or laava. The engagement ceremony can also be held as a separate event on a different day. It is usually conducted in the gurdwara or at the home of the groom-to-be. It involves ardas, kirtan, sagun (exchange of gifts) and langar. In the "sagaan" ceremony, the groom is presented with a kara, kirpan, Indian sweets, fresh fruits, dried fruits and nuts. The bride-to-be's family in turn are presented with garments and sweets.</p>
                            </div>
                        </div>
                        <hr/>
       
                <div class="row text-center col_padding">
                            <div class="col-sm-4">
                                <b>Christian marriage</b>
                            </div>
                            <div class="col-sm-4">
                                <img src="India/Images/images/christian_marriage.jpg"  width="200px" alt="Indian Marriage" class=""/>
                            </div>
                            <div class="col-sm-4">
                                 <p class="text-justify">Most Christian authorities and bodies view marriage (also called Holy Matrimony) as a state instituted and ordained by God for the lifelong relationship between one man as husband and one woman as a wife. They consider it the most intimate of human relationships, a gift from God, and a sacred institution. Protestants consider it to be sacred, holy, and even central to the community of faith, while Catholics and Eastern Orthodox Christians consider it a Sacrament. Biblically, it is to be "held in honour among all.</p>
                            </div>
                        </div>
                        <hr/>
    </div>
      
      <!--End of Family Structure And Marriage Container-->
      
      
      
      <!--Start of Greetings Container-->
      
      
    <div id="greetings" class="tab-pane fade">
      <h3>Greetings</h3>
      
      <div class="row">
          <div class="col-sm-4 fonts">
              <b>Greetings (शुभकामना)</b>
          </div>
          <div class="col-sm-4">
              <img src="India/Images/images/namaste.png"  width="140px" alt="Indian Marriage"/>
          </div>
          <div class="col-sm-4 text-justify">
              <p>Greeting is an act of communication in which human beings intentionally make their presence known to each other, to show attention to, and to suggest a type of relationship (usually cordial) or social status (formal or informal) between individuals or groups of people coming in contact with each other.</p>
          </div>
      </div>
        <hr/>
          
     <div class="row">
          <div class="col-sm-4 fonts">
              <b>Greetings in Hindi</b>
          </div>
          <div class="col-sm-4">
              <b>Namaste, Jai Sri Ram (नमस्ते)</b>
          </div>
          <div class="col-sm-4 text-justify">
              <p>Greetings include Namaste in (Hindi and Sanskrit), Namaskar (Hindi)</p>
          </div>
      </div>
        <hr/>
        
        <div class="row">
          <div class="col-sm-4 fonts">
              <b>Greetings in Odia</b>
          </div>
          <div class="col-sm-4">
              <b>Juhar/Namaskar and Jai Jagannath</b>
          </div>
          <div class="col-sm-4 text-justify">
              <p>Greetings include Juhar/Namaskar and Jai Jagannath in (Odia)</p>
          </div>
      </div>
        <hr/>
        
        <div class="row">
          <div class="col-sm-4 fonts">
              <b>Greetings in Marathi</b>
          </div>
          <div class="col-sm-4">
              <b>Namaskar</b>
          </div>
          <div class="col-sm-4 text-justify">
              <p>Greetings include Namaskar in (Marathi)</p>
          </div>
      </div>
        <hr/>
        
         <div class="row">
          <div class="col-sm-4 fonts">
              <b>Greetings in Kannada</b>
          </div>
          <div class="col-sm-4">
              <b>Namaskara</b>
          </div>
          <div class="col-sm-4 text-justify">
              <p>Greetings include Namaskara in (Kannada)</p>
          </div>
      </div>
        <hr/>
        
        <div class="row">
          <div class="col-sm-4 fonts">
              <b>Greetings in (Telugu, Malayalam)</b>
          </div>
          <div class="col-sm-4">
              <b>Namaskaram</b>
          </div>
          <div class="col-sm-4 text-justify">
              <p>Greetings include Namaskaram in (Telugu, Malayalam)</p>
          </div>
      </div>
        <hr/>
        
        <div class="row">
          <div class="col-sm-4 fonts">
              <b>Greetings in Tamil</b>
          </div>
          <div class="col-sm-4">
              <b>Vanakkam</b>
          </div>
          <div class="col-sm-4 text-justify">
              <p>Greetings include Vanakkam in (Tamil)</p>
          </div>
      </div>
        <hr/>
        
        <div class="row">
          <div class="col-sm-4 fonts">
              <b>Greetings in Bengali</b>
          </div>
          <div class="col-sm-4">
              <b>Nomoshkaar or Ami Aschi</b>
          </div>
          <div class="col-sm-4 text-justify">
              <p>Greetings include Nomoshkaar or Ami Aschi in (Bengali)</p>
          </div>
      </div>
        <hr/>
        
        <div class="row">
          <div class="col-sm-4 fonts">
              <b>Greetings in Assamese</b>
          </div>
          <div class="col-sm-4">
              <b>Nomoskar</b>
          </div>
          <div class="col-sm-4 text-justify">
              <p>Greetings include Nomoskar in (Assamese)</p>
          </div>
      </div>
        <hr/>
        
        <div class="row">
          <div class="col-sm-4 fonts">
              <b>Greetings in Gujarati</b>
          </div>
          <div class="col-sm-4">
              <b>Jai Shri Krishna</b>
          </div>
          <div class="col-sm-4 text-justify">
              <p>Greetings include Jai Shri Krishna in (Gujarati)</p>
          </div>
      </div>
        <hr/>
        
        <div class="row">
          <div class="col-sm-4 fonts">
              <b>Greetings in Punjabi</b>
          </div>
          <div class="col-sm-4">
              <b>Ram Ram or Sat Sri Akal</b>
          </div>
          <div class="col-sm-4 text-justify">
              <p>Greetings include Ram Ram or Sat Sri Akal in (Punjabi)</p>
          </div>
      </div>
        <hr/>
        
        <div class="row">
          <div class="col-sm-4 fonts">
              <b>Greetings in Urdu</b>
          </div>
          <div class="col-sm-4">
              <b>Salaam-vaalaikum or Vaalaikum-salaam</b>
          </div>
          <!--<div class="col-sm-3">
              <img src="India/Images/images/namaste.png"  width="140px" alt="Indian Marriage"/>
          </div>-->
          <div class="col-sm-4 text-justify">
              <p>Greetings include Salaam-vaalaikum or Vaalaikum-salaam in (Urdu)</p>
          </div>
      </div>
        <hr/>

    </div>
        
        <!--Ends of Greetings Container-->

      <!--Start of Festival Container-->

    <div id="festivals" class="tab-pane fade">
      <h3>Festivals</h3>
      <div class="row">
          <div class="col-sm-4 fonts">
              <b>Festivals (समारोह)</b>
          </div>
          <div class="col-sm-4">
              <img src="India/Images/images/Festivals/festivals_pics.jpg"  width="200px" alt="Indian Marriage"/>
          </div>
          <div class="col-sm-4 text-justify">
              <p>India, being a multi-cultural, multi-ethnic and multi-religious society, celebrates holidays and festivals of various religions. The three national holidays in India, the Independence Day, the Republic Day and the Gandhi Jayanti, are celebrated with zeal and enthusiasm across India.</p>
          </div>
      </div>
        <hr/>
        
        <div class="row text-center">
                            <div class="col-sm-12 fonts">
                                <b>Our Indian Festivals</b>
                            </div>
                        </div>
                        <hr/>
       
                        <div class="row text-center col_padding">
                            <div class="col-sm-3">
                                <img src="India/Images/images/Festivals/Navratri_festival_Durga_and_Rama_traditions.jpg"  width="200px" alt="Indian Marriage" class=""/>
                                
                                <br/>
                                <b>Navratri</b>
                                <br/>
                                
                                <p class="text-justify">Navratri celebrates either Durga or Rama depending on the region. Navratri (Sanskrit: नवरात्रि, literally "nine nights"), also spelled Navaratri or Navarathri, is a multi-day Hindu festival celebrated in the autumn every year.</p>
                                
                            </div>
                            
                            <div class="col-sm-3">  
                                <img src="India/Images/images/Festivals/krishna_pic.png"  width="200px" alt="Indian Marriage" class=""/>
                                
                                <br/>
                                <b>Krishna Janmashtami</b>
                                <br/>
                                
                                <p class="text-justify">Krishna Janmashtami (Devanagari कृष्ण जन्माष्टमी, Kṛṣṇa Janmāṣṭamī), also known simply as Janmashtami, is an annual Hindu festival that celebrates the birth of Krishna, the eighth avatar of Vishnu.</p>
                                
                            </div>
                            <div class="col-sm-3">
                                <img src="India/Images/images/Festivals/240px-The_Rangoli_of_Lights.jpg"  width="200px" alt="Indian Marriage" class=""/>
                                
                                <br/>
                                 <b>Diwali or Deepavali</b>
                                <br/>
                                
                                <p class="text-justify">Rangoli decorations, made using coloured powder or sand, are popular during Diwali. Also called	Deepavali</p>
                                
                            </div>
                            <div class="col-sm-3">   
                                <img src="India/Images/images/Festivals/Mahashivratree_festival.JPG"  width="200px" alt="Indian Marriage" class=""/>
                                
                                <br/>
                                 <b>Maha Shivaratri</b>
                                <br/>
                                
                                <p class="text-justify">Maha Shivaratri is a Hindu festival celebrated annually in honour of the god Shiva.</p>
                            </div>
                        </div>
                        <hr/>
        
                    <div class="row text-center col_padding">
                            <div class="col-sm-3">
                                <img src="India/Images/images/Festivals/Ganesh_spgya_2015.jpg"  width="200px" alt="Indian Marriage" class=""/>
                                
                                <br/>
                                <b>Ganesh Chaturthi</b>
                                <br/>
                                
                                <p class="text-justify">Ganesh Chaturthi (Gaṇēśa Chaturthī), also known as Vinayaka Chaturthi (Vināyaka Chaturthī), is the Hindu festival that reveres god Ganesha. A ten-day festival, it starts on the fourth day of Hindu luni-solar calendar month Bhadrapada, which typically falls in Gregorian months of August or September.</p>
                                
                            </div>
                            
                            <div class="col-sm-3">  
                                <img src="India/Images/images/Festivals/Durga_Puja_close_up.JPG"  width="200px" alt="Indian Marriage" class=""/>
                                
                                <br/>
                                <b>Durga Puja</b>
                                <br/>
                                
                                <p class="text-justify">Durga Puja, also called Durgotsava and Navaratri, is an annual Hindu festival in the Indian subcontinent that reveres the goddess Durga. It is observed in the Hindu calendar month of Ashvin, typically September or October of the Gregorian calendar.</p>
                                
                            </div>
                            <div class="col-sm-3">
                                <img src="India/Images/images/Festivals/Holi_Celebration_2013.jpg"  width="200px" alt="Indian Marriage" class=""/>
                                
                                <br/>
                                 <b>Holi</b>
                                <br/>
                                
                                <p class="text-justify">Holi (Sanskrit: होली Holī) is a Hindu spring festival celebrated in India and Nepal, also known as the "festival of colours" or the "festival of love". The festival signifies the victory of good over evil, the arrival of spring, end of winter, and for many a festive day to meet others, play and laugh, forget and forgive, and repair broken relationships.</p>
                                
                            </div>
                            <div class="col-sm-3">   
                                <img src="India/Images/images/Festivals/Rakhi_collage.jpg"  width="200px" alt="Indian Marriage" class=""/>
                                
                                <br/>
                                 <b>Raksha Bandhan</b>
                                <br/>
                                
                                <p class="text-justify">Raksha Bandhan, or simply Rakhi in many parts of India and Nepal, is a Hindu religious and secular festival. Raksha bandhan means "bond of protection". sister and brother get together, tie rakhi on wrist, perform aarti, mark tilak, brother promises to protect sister, sister feeds brother, brother gives gift, hugs</p>
                            </div>
                        </div>
                        <hr/>
        
        
    </div>
      
      <!--Ends of Festival Container-->

    <div id="animals" class="tab-pane fade heado ic-animal">  
                        <div class="row text-center col_padding">
                            <div class="col-sm-4 fonts">
                                <b>Animals</b>
                            </div>
                            <div class="col-sm-4">
                                    <img src="India/Images/animals/TIGER.jpg"  width="140px" alt="Indian Tiger" class=""/>
                                    <img src="India/Images/animals/tiger.gif"  width="200px" height="100px" alt="Indian Marriage Palkhi" class=""/>
                            </div>
                            <div class="col-sm-4 text-justify">
                                <p>India has nearly 90,000 types of animals including over 350 mammals, 1,200 bird species and 50,000 plant species.Many of these are only found on the subcontinent. These include the Asian elephant, Bengal tiger, Asiatic lion, Indian rhinoceros and several types of monkey.India is home to a variety of animal life.Apart from a handful of domesticated animals, such as cows, water buffaloes, goats, chickens, and both Bactrian and Dromedary camels, India has a wide variety of animals native to the country.</p>
                            </div>
                        </div>
                        <hr/>
                <div class="row text-center">
                            <div class="col-sm-12 fonts">
                                <b>Indian State Animals</b>
                            </div>
                        </div>
                        <hr/>
<div class="row">
    <div class="col-md-3">
      <div class="thumbnail">
        <a target="_blank">
          <img src="India/Images/animals/Blackbuck.jpg" alt="Lights" >
              <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
            <div class="capt">
          <div class="caption">
            <p>Blackbuck<br>Andhra Pradesh</p>
          </div>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/mithun.jpg" alt="Lights" >
           <div class="overlay">
    <div class="ic-animaltextinfo">Gayal are found in Nepal, India to Indochina, and the Malay Peninsula.They found in small groups and usually contain one adult male and several females and juveniles. The skin color of the head and body is blackish-brown in both sexes, and the lower portion of the limbs are white or yellowish, the shank and forehead are creamy white or yellowish in color.</div>
               </div>
             <div class="capt">
          <div class="caption">
            <p>Gayal<br>Arunachal Pradesh</p>
          </div>
            </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Rhinoceros.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">The Indian rhinoceros (Rhinoceros unicornis), also called the greater one-horned rhinoceros and great Indian rhinoceros, is a rhinoceros native to the Indian subcontinent. It is listed as Vulnerable on the IUCN Red List, as populations are fragmented and restricted to less than 20,000 km2 (7,700 sq mi).</div>
               </div>
            <div class="capt">
          <div class="caption">
            <p>One-horned rhinoceros<br>Assam</p>
          </div>
            </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Gaur.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">The gaur, also called the Indian bison, is the largest extant bovine. This species is native to the Indian subcontinent and Southeast Asia. It has been listed as Vulnerable on the IUCN Red List since 1986. Population decline in parts of its range is likely to be more than 70% during the last three generations..</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Gaur<br>Bihar</p>
          </div>
          </div>
      </div>
    </div>
  </div>
        <div class="row">
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Indian_Water_Buffalo.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">The wild water buffalo, also called Asian buffalo, Asiatic buffalo and wild Asian buffalo, is a large bovine native to the Indian Subcontinent and Southeast Asia. It has been listed as Endangered in the IUCN Red List since 1986, as the remaining population totals less than 4,000.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Wild water buffalo<br>Chhattisgarh</p>
          </div>
         </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Gaur.jpg" alt="Lights">
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">  
          <div class="caption">
            <p>Gaur<br>Goa</p>
          </div>
          </div>
      </div>
    </div>
          <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Panthera_leo_persica_male.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Asiatic lion<br>Gujarat</p>
          </div>
           </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Blackbuck.jpg" alt="Lights">
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Blackbuck<br>Haryana</p>
          </div>
          </div>
      </div>
    </div>     
  </div>
        <div class="row">
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Snow_leopard.jpg" alt="Lights">
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Snow leopard<br>Himachal Pradesh</p>
          </div>
          </div>
      </div>
    </div>        
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/hangul.jpg" alt="Lights">
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Hangul<br>Jammu and Kashmir</p>
          </div>
          </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/IndianElephant.jpg" alt="Lights">
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Indian Elephant<br>Jharkhand</p>
          </div>
          </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/IndianElephant.jpg" alt="Lights">
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Indian Elephant<br>Karnataka</p>
          </div>
          </div>
      </div>
    </div>
  </div>
   <div class="row">
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/IndianElephant.jpg" alt="Lights">
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Indian Elephant<br>Kerala</p>
          </div>
          </div>
      </div>
    </div>        
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Barasingha.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Barasingha<br>MadhyaPradesh</p>
          </div>
          </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Indian _giant_ squirrel.jpg" alt="Lights">
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Indian giant squirrel<br>Maharashtra</p>
          </div>
          </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Sangai.jpg" alt="Lights">
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Sangai<br>Manipur</p>
          </div>
          </div>
      </div>
    </div>
  </div>
   <div class="row">
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Clouded leopard.jpg" alt="Lights">
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Clouded leopard<br>Meghalaya</p>
          </div>
          </div>
      </div>
    </div>        
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Himalayan serow.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Himalayan serow<br>Mizoram</p>
          </div>
          </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/mithun.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Mithun<br>Nagaland</p>
          </div>
          </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Sambar.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Sambar<br>Odisha</p>
          </div>
          </div>
      </div>
    </div>
  </div>
   <div class="row">
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Blackbuck.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Blackbuck<br>Punjab</p>
          </div>
          </div>
      </div>
    </div>        
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Dromedary Camel.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Dromedary Camel<br>Rajasthan</p>
          </div>
          </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Red panda.jpg" alt="Lights">
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Red panda<br>Sikkim</p>
          </div>
          </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Nilgiri tahr.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Nilgiri tahr<br>Tamil Nadu</p>
          </div>
          </div>
      </div>
    </div>
  </div>
   <div class="row">
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Spotted deer.jpg" alt="Lights">
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Spotted deer<br>Telangana</p>
          </div>
          </div>
      </div>
    </div>        
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Phayre's langur.jpg" alt="Lights">
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Phayre's langur<br>Tripura</p>
          </div>
          </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Swamp deer.jpg" alt="Lights">
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Swamp deer<br>Uttar Pradesh</p>
          </div>
          </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Alpine Musk Deer.jpg" alt="Lights">
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Alpine Musk Deer<br>Uttarakhand</p>
          </div>
          </div>
      </div>
    </div>
  </div>
   <div class="row">
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Fishing cat.jpg" alt="Lights">
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Fishing cat<br>West Bengal</p>
          </div>
          </div>
      </div>
    </div>        
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Dugong.jpg" alt="Lights">
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Dugong<br>Andaman and Nicobar Islands</p>
          </div>
          </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Indian Gray Mongoose.jpg" alt="Lights">
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Indian Gray Mongoose<br>Chandigarh</p>
          </div>
          </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Nilgai.jpg" alt="Lights">
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Nilgai<br>Delhi</p>
          </div>
          </div>
      </div>
    </div>
  </div>
   <div class="row">
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Butterfly fish.jpg" alt="Lights">
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Butterfly fish<br>Lakshadweep</p>
          </div>
          </div>
      </div>
    </div>        
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/Indian palm squirrel.jpg" alt="Lights">
          <div class="overlay">
    <div class="ic-animaltextinfo">The blackbuck, also known as the Indian antelope, is an antelope found in India, Nepal and Pakistan. The blackbuck is the sole extant member of the genus Antilope. The species was described and given its binomial name by Swedish zoologist Carl Linnaeus in 1758.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Indian palm squirrel<br>Puducherry</p>
          </div>
          </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/plant.jpg" alt="Lights">
          <div class="capt">
          <div class="caption">
            <p>Not declared yet<br>Daman and Diu</p>
          </div>
          </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/animals/plant.jpg" alt="Lights">
          <div class="capt">
          <div class="caption">
            <p>Not declared yet<br>Dadra and Nagar Haveli</p>
          </div>
          </div>
      </div>
    </div>
  </div>
    </div>
      
       <!--End of Animals-->

    <div id="cuisine" class="tab-pane fade heado ic-animal">
      <h3>Cuisine</h3>
      
                      <div class="row text-center col_padding">
                            <div class="col-sm-4 fonts">
                                <b>Cuisine (पाक-प्रणाली) </b>
                            </div>
                            <div class="col-sm-4">
                                    <img src="India/Images/images/indian-cuisine2.jpg"  width="140px" alt="Indian Cuisine" class=""/>
                                    <img src="India/Images/images/indian-Cuisine.jpg"  width="200px" height="100px" alt="Indian Cuisine" class=""/>
                            </div>
                            <div class="col-sm-4 text-justify">
                                <p>Indian cuisine consists of a wide variety of regional and traditional cuisines native to the Indian subcontinent. Given the range of diversity in soil type, climate, culture, ethnic groups, and occupations, these cuisines vary substantially from each other and use locally available spices, herbs, vegetables, and fruits. Indian food is also heavily influenced by religion.Indian cuisine is still evolving, as a result of the nation's cultural interactions with other societies.</p>
                            </div>
                        </div>
                        <hr/>
                <div class="row text-center">
                            <div class="col-sm-12 fonts">
                                <b>Our Indian Cuisine</b>
                            </div>
                        </div>
                        <hr/>
<div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a target="_blank">
          <img src="India/Images/cuisins/Bengali.jpg" alt="Lights" >
              <div class="overlay">
    <div class="ic-animaltextinfo">Bengali cuisine is appreciated for its fabulous use of panchphoron, a term used to refer to the five essential spices, namely mustard, fenugreek seed, cumin seed, aniseed, and black cumin seed. The specialty of Bengali food lies in the perfect blend of sweet and spicy flavors.</div>
               </div>
            <div class="capt">
          <div class="caption">
            <p>Bengali Food</p>
          </div>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
          <img src="India/Images/cuisins/Gujarat.jpg" alt="Lights" >
           <div class="overlay">
    <div class="ic-animaltextinfo">The traditional Gujarati food is primarily vegetarian and has a high nutritional value. The typical Gujarati thali consists of varied kinds of lip smacking dishes. Gujarati cuisine has so much to offer and each dish has an absolutely different cooking style.</div>
               </div>
             <div class="capt">
          <div class="caption">
            <p>Gujarati Food</p>
          </div>
            </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
          <img src="India/Images/cuisins/Kashmiri.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo"> Kashmiri food that we have today in the restaurants has evolved over the years. Highly influenced by the traditional food of the Kashmiri pundits, it has now taken some of the features of the cooking style adopted in Central Asia, Persia and Afghanistan.</div>
               </div>
            <div class="capt">
          <div class="caption">
            <p>Kashmiri food</p>
          </div>
            </div>
      </div>
    </div>
  </div>
        <div class="row">
            <div class="col-md-4">
      <div class="thumbnail">
          <img src="India/Images/cuisins/Punjabi1.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">The cuisine of Punjab has an enormous variety of mouth-watering vegetarian as well as non vegetarian dishes. The spice content ranges from minimal to pleasant to high. Punjabi food is usually relished by people of all communities. In Punjab, home cooking differs from the restaurant cooking style.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Punjabi Food</p>
          </div>
          </div>
      </div>
    </div>
        <div class="col-md-4">
      <div class="thumbnail">
          <img src="India/Images/cuisins/Rajasthani.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">The cuisine of Rajasthan is primarily vegetarian and offers a fabulous variety of mouthwatering dishes. The spice content is quite high in comparison to other Indian cuisines, but the food is absolutely scrumptious. Rajasthanis use ghee for cooking most of the dishes. Rajasthani food is well known for its spicy curries and delicious sweets.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Rajasthani Food</p>
          </div>
          </div>
      </div>
    </div>
        <div class="col-md-4">
      <div class="thumbnail">
          <img src="India/Images/cuisins/South-indian.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">The cuisine of South India is known for its light, low calorie appetizing dishes. The traditional food of South India is mainly rice based. The cuisine is famous for its wonderful mixing of rice and lentils to prepare yummy lip smacking dosas, vadas, idlis and uttapams.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>South Indian Food</p>
          </div>
          </div>
      </div>
    </div>
     </div>
      </div>
      
       <!--End of cuisine-->


        <div id="clothing" class="tab-pane fade heado Wrapper ic-animal">      
                      <div class="row text-center col_padding">
                            <div class="col-sm-4 fonts">
                                <b>Clothing (पहनावा) </b>
                            </div>
                            <div class="col-sm-4">
                                    <img src="India/Images/clothing/clothing1.jpg"  width="140px" alt="Indian Clothes" class=""/>
                            </div>
                            <div class="col-sm-4 text-justify">
                                <p>Clothing in India varies depending on the different ethnicity, geography, climate and cultural traditions of the people of each region of India. Historically, male and female clothing has evolved from simple kaupinam, langota, dhoti, lungi, saree, gamucha, and loincloths to cover the body to elaborate costumes not only used in daily wear but also on festive occasions as well as rituals and dance performances. In urban areas, western clothing is common and uniformly worn by people of all social levels.</p>
                            </div>
                        </div>
                        <hr/>
                <div class="row text-center">
                            <div class="col-sm-12 fonts">
                                <b>Our Indian Clothing</b>
                            </div>
                        </div>
                        <hr/>
<div class="row">
    <div class="col-md-3">
      <div class="thumbnail ">
        <a target="_blank">
          <img src="India/Images/clothing/saree1.jpg" alt="Lights" >
              <div class="overlay">
    <div class="ic-animaltextinfo"> A saree or sari is a female garment in the Indian subcontinent.A sari is a strip of unstitched cloth, ranging from four to nine meters in length, that is draped over the body in various styles.Sari is one of the most wonderful dresses worn by Indian women. Saree is an unstitched piece of clothing usually four to nine metres long depending on the style in which you want to drape it.</div>
               </div>
            <div class="capt">
          <div class="caption">
            <p>Indian Saree</p>
          </div>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/clothing/salwar-kameez.jpg" alt="Lights" >
           <div class="overlay">
    <div class="ic-animaltextinfo"> Salwar is a generic description of the lower garment incorporating the Punjabi salwar, Sindhi suthan, Dogri pajamma (also called suthan) and the Kashmiri suthan.The salwar kameez has become the most popular dress for females. It consists of loose trousers (the salwar) narrow at the ankles, topped by a tunic top (the kameez). Women generally wear a dupatta or odani (Veil) with salwar kameez to cover their head and shoulders.</div>
               </div>
             <div class="capt">
          <div class="caption">
            <p>Salwar</p>
          </div>
            </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/clothing/Lehenga%20Choli.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo"> A Ghagra Choli or a Lehenga Choli is the traditional clothing of women in Rajasthan and Gujarat. Punjabis also wear them and they are used in some of their folk dances. It is a combination of lehenga, a tight choli and an odhani. A lehenga is a form of a long skirt which is pleated. It is usually embroidered or has a thick border at the bottom. A choli is a blouse shell garment, which is cut to fit to the body and has short sleeves and a low neck.</div>
               </div>
            <div class="capt">
          <div class="caption">
            <p>Lehenga Choli</p>
          </div>
            </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/clothing/Pattu%20Pavadai.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">Pattu Pavadai or Langa davani is a traditional dress in south India and Rajasthan, usually worn by teenage and small girls. The pavada is a cone-shaped skirt, usually of silk, that hangs down from the waist to the toes. It normally has a golden border at the bottom.Girls in south India often wear pattu pavadai or Langa davani during traditional functions.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Pattu Pavadai</p>
          </div>
          </div>
      </div>
    </div>
    </div>
    <div class="row">
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/clothing/mekhela%20sado.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">Mekhela Sador is the traditional Assamese dress worn by women. It is worn by women of all ages.There are three main pieces of cloth that are draped around the body.The bottom portion, draped from the waist downwards is called the Mekhela.The top portion of the three-piece dress, called the Sador.The third piece is called a Riha, which is worn under the Sador. It is narrow in width.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Mekhela Sador</p>
          </div>
          </div>
      </div>
    </div>
        <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/clothing/Indian-Churidar-Kameez.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">Churidaar is a variation on the salwar, loose above the knees and tightly fitted to the calf below. While the salwar is baggy and caught in at the ankle, the churidar fits below the knees with horizontal gathers near the ankles. The churidaar can be worn with any upper garment such as a long kurta, which goes below the knees, or as part of the anarkali suit.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Indian Churidaar</p>
          </div>
          </div>
      </div>
    </div>
                <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/clothing/Dhavani.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">This is a type of South Indian dress mainly worn in Karnataka, Andhra Pradesh, and Tamil Nadu, as well as in some parts of Kerala. This dress is a three-piece garment where the lengha or lehenga is the cone shaped long flowing skirt.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Dhavani</p>
          </div>
          </div>
      </div>
    </div>        <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/clothing/anarkali%20suit.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">The anarkali suit is made up of a long, frock-style top and features a slim fitted bottom.The anarkali is an extremely desirable style that is adorned by women located in Northern India, Pakistan and The Middle East. The anarkali suit varies in many different lengths and embroideries including floor length anarkali styles. Many women will also opt for heavier embroidered anarkali suits on wedding functions and events.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Anarkali Suit</p>
          </div>
          </div>
      </div>
    </div>
     </div>
    <div class="row">
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/clothing/dhoti.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">Dhoti is the national dress of India. A dhoti is from four to six feet long white or colour strip of cotton. This traditional attire is mainly worn by men in villages.[43] It is held in place by a style of wrapping and sometimes with the help of a belt, ornamental and embroidered or a flat and simple one, around the waist.The cultural icons such as the classical musicians, dancers and poets can be quite often seen clad in dhoti kurta. They derive pride in exhibiting the rich culture of their country.
            </div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Indian Dhoti</p>
          </div>
          </div>
      </div>
    </div>
        <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/clothing/lungi.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">A Lungi, also known as sarong, is a traditional garment of India. A Mundu is a lungi, except that it is always white. It is either tucked in, over the waist, up to knee-length or is allowed to lie over and reach up to the ankle. It is usually tucked in when the person is working, in fields or workshops, and left open usually as a mark of respect, in worship places or when the person is around dignitaries.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Indian Lungi</p>
          </div>
          </div>
      </div>
    </div>
                <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/clothing/Sherwan.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">An Achkan or a Sherwani is a long coat / jacket that usually sports exposed buttons through the length of the jacket. The length is usually just below the knees and the jacket ends just below the knee. The jacket has a Nehru collar, which is a collar that stands up. The Achkan is worn with tight fitting pants or trousers called churidars. Churidars are trousers that are loose around the hips and thighs, but are tight and gathered around the ankle.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Sherwani</p>
          </div>
          </div>
      </div>
    </div>        <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/clothing/Bandhgala.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo"> A Jodhpuri or a Bandhgala is a formal evening suit from India. It originated in the Jodhpur State, and was popularized during the British Raj in India. Also known as Jodhpuri Suit, it is a western style suit product, with a coat and a trouser, at times accompanied by a vest. It brings together the western cut with Indian hand-embroidery escorted by the Waist coat.It is suitable for occasions such as weddings and formal gatherings.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Bandhgala</p>
          </div>
          </div>
      </div>
    </div>
     </div>
        <div class="row">
    <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/clothing/Angarakha.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo"> The term angarkha is derived from the Sanskrit word Aṅgarakṣaka, which means protection of the body.The angarkha was worn in various parts of the Indian Subcontinent, but while the basic cut remained the same, styles and lengths varied from region to region. Angarakha is a traditional upper garment worn in the Indian Subcontinent which overlap and are tied to the left or right shoulder.
            </div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Angarakha</p>
          </div>
          </div>
      </div>
    </div>
        <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/clothing/dastar.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo">The Dastar, also known as a pagri, is a turban worn by the Sikh community of India. Is a symbol of faith representing values such as valour, honour and spirituality among others. It is worn to protect the Sikh's long, uncut hair, the Kesh which is one of the Five Ks of Sikhism.Over the years, the dastar has evolved into different styles pertaining to the various sects of Sikhism such as the Nihang and the Namdhari.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Dastar</p>
          </div>
          </div>
      </div>
    </div>
                <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/clothing/Pheta.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo"> Pheta is the Marathi name for turbans worn in the state of Maharashtra. It's usually worn during traditional ceremonies and occasions. It was a mandatory part of clothing in the past and have evolved into various styles in different regions. The main types are the Puneri Pagadi, Kolhapuri and Mawali pheta.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Pheta</p>
          </div>
          </div>
      </div>
    </div>        <div class="col-md-3">
      <div class="thumbnail">
          <img src="India/Images/clothing/Gandhi%20cap.jpg" alt="Lights" >
          <div class="overlay">
    <div class="ic-animaltextinfo"> The Gandhi cap, a white coloured cap made of khadi was popularised by Mahatma Gandhi during the Indian independence movement. The practice of wearing a Gandhi cap was carried on even after independence and became a symbolic tradition for politicians and social activists. The cap has been worn throughout history in many states such as Gujarat, Maharashtra, Uttar Pradesh and West Bengal and is still worn by many people without political significance.</div>
               </div>
          <div class="capt">
          <div class="caption">
            <p>Gandhi Cap</p>
          </div>
          </div>
      </div>
    </div>
     </div>
    </div>
      

<div id="languages_and_literature" class="tab-pane fade heado Wrapper ic-animal">      
                      <div class="row text-center col_padding">
                            <div class="col-sm-4 fonts">
                                <b>Language (भाषा) </b>
                            </div>
                            <div class="col-sm-4">
                                    <img src="India/Images/clothing/language.png"  width="140px" alt="Indian languages" class=""/>
                            </div>
                            <div class="col-sm-4 text-justify">
                                <p>Languages spoken in India belong to several language families, the major ones being the Indo-Aryan languages spoken by 78.05% of Indians and the Dravidian languages spoken by 19.64% of Indians.Languages spoken by the remaining 2.31% of the population belong to the Austroasiatic, Sino-Tibetan, Tai-Kadai, and a few other minor language families and isolates.India (780) has the world's second highest number of languages, after Papua New Guinea (839).Article 343 of the Indian constitution stated that the official language of the Union should become Hindi in Devanagari script instead of the extant English.</p>
                            </div>
                        </div>
                        <hr/>
                <div class="row text-center">
                            <div class="col-sm-12 fonts">
                                <b>First, second, and third languages of India</b>
                            </div>
                        </div>
                        <hr/>
                      <div class="row text-center col_padding">
                            <div class="col-sm-6 fonts">
                                <b>Hindi</b>
                            </div>
                            <div class="col-sm-6 text-justify">
                                <p>Hindi, or Modern Standard Hindi is a standardised and Sanskritised register of the Hindustani language. Along with the English language, Hindi written in the Devanagari script is the official language of India.Hindi is the lingua franca of the Hindi belt, and to a lesser extent the whole of India.</p>
                            </div>
                        </div>
                        <hr/>
                      <div class="row text-center col_padding">
                            <div class="col-sm-6 fonts">
                                <b>English</b>
                            </div>
                            <div class="col-sm-6 text-justify">
                                <p>English is a West Germanic language that was first spoken in early medieval England and is now a global lingua franca. Named after the Angles, one of the Germanic tribes that migrated to the area of Britain that would later take their name, England, both names ultimately deriving from the Anglia peninsula in the Baltic Sea. It is closely related to the Frisian languages, but its vocabulary has been significantly influenced by other Germanic languages, particularly Norse, as well as by Latin and French.</p>
                            </div>
                        </div>
                        <hr/>
                      <div class="row text-center col_padding">
                            <div class="col-sm-6 fonts">
                                <b>Bengali</b>
                            </div>
                            <div class="col-sm-6 text-justify">
                                <p>Bengali, also known by its endonym Bangla, is an Indo-Aryan language spoken in South Asia. It is the official and most widely spoken language of Bangladesh and second most widely spoken of the 22 scheduled languages of India, behind Hindi.</p>
                            </div>
                        </div>
                        <hr/>
                      <div class="row text-center col_padding">
                            <div class="col-sm-6 fonts">
                                <b>Telugu</b>
                            </div>
                            <div class="col-sm-6 text-justify">
                                <p>Telugu is a South-central Dravidian language native to India. It stands alongside Hindi, English and Bengali as one of the few languages with official primary language status in more than one Indian state. Telugu is the primary language in the states of Andhra Pradesh, Telangana, and the union territory of Puducherry. There are also significant linguistic minorities in neighbouring states. It is one of six languages designated a classical language of India by the country's government.</p>
                            </div>
                        </div>
                        <hr/>
                      <div class="row text-center col_padding">
                            <div class="col-sm-6 fonts">
                                <b>Marathi</b>
                            </div>
                            <div class="col-sm-6 text-justify">
                                <p>Marathi is an Indo-Aryan language spoken predominantly by around 83 million Marathi people of Maharashtra, India. It is the official language and co-official language in the Maharashtra and Goa states of Western India, respectively, and is one of the 22 scheduled languages of India. There were 83 million speakers in 2011; Marathi ranks 19th in the list of most spoken languages in the world. Marathi has the third largest number of native speakers in India, after Hindi and Bengali. Marathi has some of the oldest literature of all modern Indian languages, dating from about 900 AD. The major dialects of Marathi are Standard Marathi and the Varhadi dialect. Koli, Malvani Konkani has been heavily influenced by Marathi varieties.</p>
                            </div>
                        </div>
                        <hr/>
                      <div class="row text-center col_padding">
                            <div class="col-sm-6 fonts">
                                <b>Tamil</b>
                            </div>
                            <div class="col-sm-6 text-justify">
                                <p>Tamil is a Dravidian language predominantly spoken by the Tamil people of India and Sri Lanka, and by the Tamil diaspora, Sri Lankan Moors, Burghers, Douglas, and Chindians. Tamil is an official language of two countries: Sri Lanka and Singapore. It has official status in the Indian state of Tamil Nadu and the Indian Union Territory of Puducherry. It is used as one of the languages of education in Malaysia, along with English, Malay and Mandarin. Tamil is spoken by significant minorities in the four other South Indian states of Kerala, Karnataka, Andhra Pradesh and Telangana and the Union Territory of the Andaman and Nicobar Islands. It is one of the 22 scheduled languages of India.</p>
                            </div>
                        </div>
                        <hr/>
                      <div class="row text-center col_padding">
                            <div class="col-sm-6 fonts">
                                <b>Urdu</b>
                            </div>
                            <div class="col-sm-6 text-justify">
                                <p>Urdu is a Persianised standard register of the Hindustani language. It is the official national language and lingua franca of Pakistan. In India, it is one of the 22 official languages recognized in the Constitution of India, having official status in the six states of Jammu and Kashmir, Telangana, Uttar Pradesh, Bihar, Jharkhand and West Bengal, as well as the national capital territory of Delhi. It is a registered regional language of Nepal.</p>
                            </div>
                        </div>
                        <hr/>
                      <div class="row text-center col_padding">
                            <div class="col-sm-6 fonts">
                                <b>Kannada</b>
                            </div>
                            <div class="col-sm-6 text-justify">
                                <p>Kannada is a Dravidian language spoken predominantly by Kannada people in India, mainly in the state of Karnataka, and by significant linguistic minorities in the states of Andhra Pradesh, Telangana, Tamil Nadu, Maharashtra, Kerala, Goa and abroad. The language has roughly 43.7 million native speakers, who are called Kannadigas. Kannada is also spoken as a second and third language by over 12.9 million non-Kannada speakers living in Karnataka, which adds up to 56.6 million speakers. It is one of the scheduled languages of India and the official and administrative language of the state of Karnataka.</p>
                            </div>
                        </div>
                        <hr/>
                      <div class="row text-center col_padding">
                            <div class="col-sm-6 fonts">
                                <b>Gujarati</b>
                            </div>
                            <div class="col-sm-6 text-justify">
                                <p>Gujarati is an Indo-Aryan language native to the Indian state of Gujarat. It is part of the greater Indo-European language family. Gujarati is descended from Old Gujarati. In India, it is the official language in the state of Gujarat, as well as an official language in the union territories of Daman and Diu and Dadra and Nagar Haveli. As of 2011, Gujarati is the 6th most widely spoken language in India by number of native speakers, spoken by 55.5 million speakers which amounts to about 4.5% of the total Indian population. It is the 26th most widely spoken language in the world by number of native speakers as of 2007.</p>
                            </div>
                        </div>
                        <hr/>
                      <div class="row text-center col_padding">
                            <div class="col-sm-6 fonts">
                                <b>Odia</b>
                            </div>
                            <div class="col-sm-6 text-justify">
                                <p>Odia is a language spoken by 4.2% of India's population. It is a classical Indo-Aryan language that is spoken mostly in eastern India, with around 40 million native speakers globally.Odia is one of the many official languages of India; it is the official language of Odisha and the second official language of Jharkhand.Odia is the sixth Indian language to be designated a Classical Language in India on the basis of having a long literary history and not having borrowed extensively from other languages.</p>
                            </div>
                        </div>
                        <hr/>
                      <div class="row text-center col_padding">
                            <div class="col-sm-6 fonts">
                                <b>Malayalam</b>
                            </div>
                            <div class="col-sm-6 text-justify">
                                <p>Malayalam is a Dravidian language spoken across the Indian state of Kerala by the Malayali people and it is one of 22 scheduled languages of India. Designated a "Classical Language in India" in 2013, it was developed into the current form mainly by the influence of the poet Thunchaththu Ezhuthachan in the 16th century. Malayalam has official language status in the state of Kerala and in the union territories of Lakshadweep and Puducherry. It belongs to the Dravidian family of languages and is spoken by 38 million people. Malayalam is also spoken by linguistic minorities in the neighbouring states; with significant number of speakers in the Nilgiris, Kanyakumari and Coimbatore districts of Tamil Nadu, and Dakshina Kannada district of Karnataka. Malayalam serves as a link language on certain islands, including the Mahl-dominated Minicoy Island.</p>
                            </div>
                        </div>
                        <hr/>
                      <div class="row text-center col_padding">
                            <div class="col-sm-6 fonts">
                                <b>Sanskrit</b>
                            </div>
                            <div class="col-sm-6 text-justify">
                                <p>Sanskrit is a language of ancient India with a documented history of nearly 3,500 years. It is the primary liturgical language of Hinduism; the predominant language of most works of Hindu philosophy as well as some of the principal texts of Buddhism and Jainism. Sanskrit, in its various variants and dialects, was the lingua franca of ancient and medieval India. In the early 1st millennium CE, along with Buddhism and Hinduism, Sanskrit migrated to Southeast Asia, parts of East Asia and Central Asia, emerging as a language of high culture and of local ruling elites in these regions.</p>
                            </div>
                        </div>
                        <hr/>
    </div>

    <div id="performing_arts_and_visual_arts" class="tab-pane fade">
      <h3>Menu 3</h3>
      <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
    </div>

  </div>
                        
  </div>            <!--End of container-fluid-->           
                        
        </main>
        
        <footer>
            <?php
            include("footer.php");
            ?>
        </footer>
        
    </body>
</html>