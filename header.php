<!DOCTYPE html>
<html lang="en">
<head>  
    <style>
    /* Set black background color, white text and some padding */
    .header {
      background-color: #F8981C;
      color: white;
      padding: 15px;
    }
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
      background-color: #F8981C;
    }
  </style>
</head>
    
    <body>
         <!-- header part -->
            <header class="container-fluid header">
                
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            
                            <div class="flag">
                                <a href="#"><img src="India/Images/Animated_Images/India_logo.gif" class="img-responsive"/></a>
                            </div>
                    
                            <div class="indianculture"><span>The Indian Cultures</span></div> 
                            
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-md-4 headName text-center"><span>भारतीय संस्कृति</span></div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <!--<div class="gold_birds">
                                <img src="India/Images/Animated_Images/gold_birds.gif" width="100px"/>
                            </div>-->
                        </div>
                    </div>
           </header>
        <!-- End of header part -->
        
         <!--nav part -->
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
                            <div class="AshokChakra">
                                <a class="navbar-brand" href="#">
                                    <img src="India/Images/IndianLogo/80px-Ashoka_Chakra.svg.png" width="30px;" height="25px;" />
                                </a>
                            </div>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">State <span class="caret"></span></a>
          <ul class="dropdown-menu tic-ddmenu">
                        <h4 class="tic-stateddheading">State</h4>
                        <li><a href="#">Andhra Pradesh</a></li>
                        <li><a href="#">Arunachal Pradesh</a></li>
                        <li><a href="#">Assam</a></li>
                        <li><a href="#">Bihar</a></li>
                        <li><a href="#">Chhattisgarh</a></li>
                        <li><a href="#">Goa</a></li>
                        <li><a href="#">Gujarat</a></li>
                        <li><a href="#">Haryana</a></li>
                        <li><a href="#">Himachal Pradesh</a></li>
                        <li><a href="#">Jammu and Kashmir</a></li>
                        <li><a href="#">Jharkhand</a></li>
                        <li><a href="#">Karnataka</a></li>
                        <li><a href="#">Kerala</a></li>
                        <li><a href="#">Madhya Pradesh</a></li>
                        <li><a href="#">Maharashtra</a></li>
                        <li><a href="#">Manipur</a></li>
                        <li><a href="#">Meghalaya</a></li>
                        <li><a href="#">Mizoram</a></li>
                        <li><a href="#">Nagaland</a></li>
                        <li><a href="#">Odisha</a></li>
                        <li><a href="#">Punjab</a></li>
                        <li><a href="#">Rajasthan</a></li>
                        <li><a href="#">Sikkim</a></li>
                        <li><a href="#">Tamil Nadu</a></li>
                        <li><a href="#">Telangana</a></li>
                        <li><a href="#">Tripura</a></li>
                        <li><a href="#">Uttar Pradesh</a></li>
                        <li><a href="#">Uttarakhand</a></li>
                        <li><a href="#">West Bengal</a></li>
                        <h4 class="tic-utddheading">Union territories</h4>
                        <li><a href="#">Andaman and Nicobar Islands</a></li>
                        <li><a href="#">Chandigarh</a></li>
                        <li><a href="#">Dadra and Nagar Haveli</a></li>
                        <li><a href="#">Daman and Diu</a></li>
                        <li><a href="#">Lakshadweep</a></li>
                        <li><a href="#">National Capital Territory of Delhi</a></li>
                        <li><a href="#">Puducherry</a></li>
          </ul>
        </li>
        <li><a href="#">Events</a></li>
        <li><a href="#">Festivals</a></li>
        <li><a href="#">News</a></li>
        <li><a href="#">Gallery</a></li>
        <li><a href="#">Blogs</a></li>
        <li><a href="#">About Us</a></li>
        <li><a href="#">Contact Us</a></li>
      </ul>
     <!--  <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul> -->
    </div>
  </div>
</nav>
     <!-- End of nav part -->
    </body>
</html>


